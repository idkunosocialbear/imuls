var fs = require("fs");
var rp = require('request-promise');
const address = '6512503f.ngrok.io';
const iterations = 100;
const filen = './s.jpeg' //replace with the path to the test file

async function runTests() {
    console.time('execution');
    var requests = [];
    for (var i = 0; i < iterations; i++) {
        requests.push(makeRequest());
    }
    await Promise.all(requests);
    console.timeEnd('execution');
}

async function makeRequest() {
    var options = {
        method: 'POST',
        uri: 'http://' + address + '/photos/upload',
        headers: {
            'Postman-Token': '5d86a1de-1527-482c-a6d3-df46bbe2c795',
            'Cache-Control': 'no-cache',
            'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
        },
        formData: {
            file: {
                value: fs.createReadStream(filen),
                options: {
                    filename: filen,
                    contentType: null
                }
            }
        }
    };

    return rp(options)
        .then(function(body) {
            // POST succeeded...
            console.log(body);
            return true;
        })
        .catch(function(err) {
            // POST failed...
            throw new Error(err)
        });
}

runTests();