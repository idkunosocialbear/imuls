const gulp = require('gulp');
const javascriptObfuscator = require('gulp-javascript-obfuscator');


function defaultTask(cb) {
    // place code for your default task here
gulp.src('./src/app.js')
    .pipe(javascriptObfuscator())
    .pipe(gulp.dest('build'));
    cb();
}

exports.default = defaultTask

