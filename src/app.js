const express = require('express');
const path = require('path');
const app = express();
const ip = require('ip');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, '/build/img')
    },
    filename: function(req, file, cb) {
        console.time('execution');
        let nfn = Date.now() + ID() + path.extname(file.originalname);
        console.log(file.originalname + " > " + nfn);
        file.nfn = nfn;
        cb(null, nfn)
        console.timeEnd('execution');
    }
});
const upload = multer({ storage: storage });
app.use(express.static(path.join(__dirname, 'public')));

const server = app.listen(3030, function() {
    console.log('Listening on port %d', server.address().port);
    console.log('Address is : ' +
        ip.address())
});

const ID = function() {
    return '_' + Math.random().toString(36).substr(2, 9);
};

app.post('/photos/upload', upload.single('file'), function(req, res, next) {
    let s = req.file ? true : false;
    let m = req.file ? req.file.nfn : 'No image uploaded';
    return res.status(200).send({ success: s, message: m });
})